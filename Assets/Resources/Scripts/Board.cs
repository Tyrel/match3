﻿using UnityEngine;
using System.Collections;

using System.Collections.Generic;



public class Board : MonoBehaviour {
	public List<Gem> gems = new List<Gem>();
	public int GridWidth;
	public int GridHeight;

	public GameObject gemPrefab;
	public Gem lastGem;
	// Animation
	public Vector3 gem1Start, gem1End, gem2Start, gem2End;
	public bool isSwapping = false;
	public Gem gem1, gem2;
	public float startTime;
	public float swapRate = 0.5f;
	public int AmountToMatch = 3;
	public bool isMatched = false;
	int topLine = 6;




	// Use this for initialization
	void Start () {
		for (int h = 0; h < GridHeight; h++) {
			for (int w = 0; w < GridWidth; w++){
				GameObject g = Instantiate(gemPrefab, new Vector3(h,w,0), Quaternion.identity) as GameObject;
				g.transform.parent = gameObject.transform;
				gems.Add(g.GetComponent<Gem>());
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (isMatched) {
			for (int i =0; i<gems.Count; i++) {
				if (gems [i].isMatched) {
					// Move it back up top, and change the color.
					gems [i].CreateGem ();
					gems [i].transform.position = new Vector3 (gems [i].transform.position.x,
					                                         gems [i].transform.position.y + topLine,
					                                         gems [i].transform.position.z);
				}
			}
			isMatched = false;
		} else if (isSwapping) {
			MoveGem (gem1, gem1End, gem1Start);
			MoveNegGem (gem2, gem2End, gem2Start);
			if (Vector3.Distance (gem1.transform.position, gem1End) < 0.1f ||
				Vector3.Distance (gem2.transform.position, gem2End) < 0.1f) {
				gem1.transform.position = gem1End;
				gem2.transform.position = gem2End;
				lastGem = null;
				gem1.ToggleSelector ();
				gem2.ToggleSelector ();
				isSwapping = false;
				TogglePhysics (false);
				CheckMatch ();
			}
		} else if (!BoardInMotion()) {
			for(int i=0;i<gems.Count;i++){
				CheckForNearbyMatches(gems[i]);
			}
		}
	}

	public bool BoardInMotion(){
		for (int i=0; i<gems.Count; i++) {
			if(gems[i].transform.localPosition.y > topLine) {
				return true;
			}
			if (gems[i].GetComponent<Rigidbody>().velocity.y > 0.1f){
				return true;
			}
		}

		return false;
	}

	public void CheckForNearbyMatches (Gem g){
		List<Gem> gemList = new List<Gem> ();
		ConstructMatchList (g.color, g, g.XCoord, g.YCoord, ref gemList);
		FixMatchList (g, gemList);
	}

	public void ConstructMatchList(string color, Gem gem, int XCoord, int YCoord, ref List<Gem> MatchList){
		if (gem == null || gem.color != color || MatchList.Contains(gem)){
			return;
		}

		MatchList.Add (gem);

		if (XCoord == gem.XCoord || YCoord == gem.YCoord){
			foreach(Gem g in gem.Neighbors){
				ConstructMatchList(color, g, XCoord, YCoord, ref MatchList);
			}
		}

	}


	public void CheckMatch(){
		List<Gem> gem1List = new List<Gem> ();
		List<Gem> gem2List = new List<Gem> ();
		ConstructMatchList (gem1.color, gem1, gem1.XCoord, gem1.YCoord, ref gem1List);
		FixMatchList (gem1, gem1List);
		ConstructMatchList (gem2.color, gem2, gem2.XCoord, gem2.YCoord, ref gem2List);
		FixMatchList (gem2, gem2List);
	}


	public void FixMatchList(Gem gem, List<Gem> ListToFix){
		List<Gem> rows = new List<Gem> ();
		List<Gem> columns = new List<Gem> ();

		for (int i=0; i <ListToFix.Count; i++) {
			if (gem.XCoord == ListToFix[i].XCoord){
				rows.Add(ListToFix[i]);
			}
			if (gem.YCoord == ListToFix[i].YCoord){
				columns.Add(ListToFix[i]);
			}
		}

		if (rows.Count >= AmountToMatch) {
			isMatched = true;
			for (int i=0;i<rows.Count;i++){
				rows[i].isMatched=true;
			}
		}

		if (columns.Count >= AmountToMatch) {
			isMatched = true;
			for (int i=0;i<columns.Count;i++){
				columns[i].isMatched=true;
			}
		}

	}


	public void MoveGem(Gem gemToMove, Vector3 toPos, Vector3 fromPos){
		Vector3 center = (fromPos + toPos) * 0.5f;
		center -= new Vector3 (0, 0, 0.1f);
		Vector3 riseRelCenter = fromPos - center;
		Vector3 setRelCenter = toPos - center;
		float fracComplete = (Time.time - startTime) / swapRate;
		gemToMove.transform.position = Vector3.Slerp (riseRelCenter, setRelCenter, fracComplete);
		gemToMove.transform.position += center;

	}

	public void MoveNegGem(Gem gemToMove, Vector3 toPos, Vector3 fromPos){
		Vector3 center = (fromPos + toPos) * 0.5f;
		center -= new Vector3 (0, 0, -0.1f);
		Vector3 riseRelCenter = fromPos - center;
		Vector3 setRelCenter = toPos - center;
		float fracComplete = (Time.time - startTime) / swapRate;
		gemToMove.transform.position = Vector3.Slerp (riseRelCenter, setRelCenter, fracComplete);
		gemToMove.transform.position += center;
	}


	public void SwapGems(Gem currentGem){
		if (lastGem == null) {
			lastGem = currentGem;
		} else if (lastGem == currentGem) {
			lastGem = null;
		} else {
			if(lastGem.IsNeighborWith(currentGem)){
				gem1Start = lastGem.transform.position;
				gem1End = currentGem.transform.position;

				gem2Start = currentGem.transform.position;
				gem2End = lastGem.transform.position;

				startTime = Time.time;

				TogglePhysics(true);
				gem1 = lastGem;
				gem2 = currentGem;
				isSwapping = true;
			} else {
				lastGem.ToggleSelector();
				lastGem	= currentGem;
			}

		}
	}

	public void TogglePhysics(bool isOn){
		for(int i = 0; i<gems.Count; i++){
			gems[i].GetComponent<Rigidbody>().isKinematic = isOn;
		}
	}
}
