﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Gem : MonoBehaviour {
	
	public List<Gem> Neighbors = new List<Gem>();
	string[] gemMats = {"Red", "Green", "Blue", "Orange", "Black", "Purple", "Yellow"};
	public string color = "";
	public GameObject gemSphere;
	public GameObject selector;
	public bool isSelected;
	public bool isMatched;


	public int XCoord {
		get {
			return Mathf.RoundToInt(transform.localPosition.x);
		}
	}
	public int YCoord {
		get {
			return Mathf.RoundToInt(transform.localPosition.y);
		}
	}



	// Use this for initialization
	void Start () {
		CreateGem ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void OnMouseDown(){
		if (!GameObject.Find ("Board").GetComponent<Board> ().isSwapping) {
			ToggleSelector ();
			GameObject.Find ("Board").GetComponent<Board> ().SwapGems (this);
		}
	}

	public void ToggleSelector(){
		isSelected = !isSelected;
		selector.SetActive (isSelected);
		
	}

	public bool IsNeighborWith(Gem g){
		if (Neighbors.Contains (g)) {
			return true;
		}
		return false;
	}


	public void AddNeighbor(Gem g){
		if(!Neighbors.Contains (g)){
			Neighbors.Add (g);
		}
	}
	
	public void RemoveNeighbor(Gem g){
		Neighbors.Remove (g);
	}
	
	public void CreateGem(){
		color = gemMats [Random.Range (0, gemMats.Length)];
			
		Material m = Resources.Load ("Materials/" + color) as Material;
		gemSphere.GetComponent<Renderer> ().material = m;
		isMatched = false;
	}
}